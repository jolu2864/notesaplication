package Model;

/*
    purpose: Contains Comments and which folder it comes from.
 */
public class Notes {
    private int notesID;
    private String comment;

    /*
        Constructor to set comment
        @param comment
     */
    Notes(String comment, int notesID)
    {
        setComment(comment);
        setNotesID(notesID);
    }

    /*
        Setter
        @param comment
     */
    private void setComment(String comment)
    {
        this.comment = comment;
    }
    /*
        Setter
        Sets notesID from static variable notesId from folder
     */
    private void setNotesID(int notesID)
    {
        this.notesID = notesID;
    }
    /*
        Getter
        @return String comment
     */
    public String getComment()
    {
        return comment;
    }
    /*
        Getter
        @return notesID
     */
    public int getNotesID()
    {
        return notesID;
    }
}
