package Model; /**
 * Created by John on 11/19/2015.
 */
import java.util.Vector;
public class Folder {
    private int currNotesID;
    private String title;
    private Vector<Notes> Vnotes = new Vector<>();

    /*
        Constructor
        Sets title
        @param Sting title
     */
    Folder(String title)
    {
        setTitle(title);
        currNotesID = 0;
    }
    /*
        Setter
        @param String title
     */
    private void setTitle(String title)
    {
        this.title = title;
    }
    /*
        Search Notes
        Traverse through Vnotes and match ID's until found
        @return index


        need to TEST THIS!!!!
     */
    private int searchNotes(int num)
    {
        int index = 0;
        for(Notes x: Vnotes)
        {
            if(num == x.getNotesID())
            {
                return index;
            }
            index++;
        }
        return index;
    }
    /*
        Getter
        @return String title
     */
    public String getTitle()
    {
        return title;
    }
    /*
        purpose: Creates Note object to be store in the vector vNotes
        @param Sting comment
     */
    public void addNote(String comment)
    {
        Vnotes.add(new Notes(comment,currNotesID));
        currNotesID++;
    }
    /*
        purpose: Removes Note from Vector vNotes
     */
    public void removeNote()
    {
        //fix search first
        //vector.remove(search(notes))
        currNotesID--;
    }
    /*
        Getter
        @return Vector<Notes> Vnotes
     */
    public Vector<Notes> getNotes()
    {
        return Vnotes;
    }



}
